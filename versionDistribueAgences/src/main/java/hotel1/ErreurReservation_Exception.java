package hotel1;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "ErreurReservation", targetNamespace = "http://service/")
public class ErreurReservation_Exception
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private ErreurReservation faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public ErreurReservation_Exception(String message, ErreurReservation faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public ErreurReservation_Exception(String message, ErreurReservation faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: hotel1.ErreurReservation
     */
    public ErreurReservation getFaultInfo() {
        return faultInfo;
    }

}
