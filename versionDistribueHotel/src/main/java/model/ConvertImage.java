package model;

import java.io.File;
import java.io.IOException;
import java.util.Base64;

import org.apache.commons.io.FileUtils;

public class ConvertImage {

	public String ImageToString(String nomImage) throws IOException {
		byte[] fileContent = FileUtils.readFileToByteArray(new File("src/main/resources/"+nomImage+".jpg"));
		String encodedString = Base64.getEncoder().encodeToString(fileContent);
		return encodedString;
	}
	
	public void StringToImage(String codeImage,String nomImage) throws IOException {	
		
		byte[] decodedBytes = Base64.getDecoder().decode(codeImage);		
		FileUtils.writeByteArrayToFile(new File("src/main/resources/"+nomImage+".jpg"), decodedBytes);
	}
	
}


