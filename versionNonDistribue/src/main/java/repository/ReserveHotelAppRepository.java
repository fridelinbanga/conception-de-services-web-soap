package repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import model.Chambre;
import model.Client;
import model.Hotel;

public class ReserveHotelAppRepository {
	
	/* ATTRIBUTES */
	private List<Hotel> hotels ;
	private List<Chambre> chambres ;

	/* CONSTRUCTOR */
	public ReserveHotelAppRepository() {
		
		/*CREATION DES CHAMBRES */
		chambres = new ArrayList<>();
		chambres.addAll(Arrays.asList(
				new Chambre(0,2,90),
				new Chambre(1,4,120),
				new Chambre(2,1,30),
				new Chambre(3,2,40),
				new Chambre(4,2,40),
				new Chambre(5,1,30),
				new Chambre(6,4,90),
				new Chambre(7,2,60),
				new Chambre(8,1,90),
				new Chambre(9,1,70),
				new Chambre(10,2,90),
				new Chambre(11,2,90),
				new Chambre(12,4,330),
				new Chambre(13,2,80),
				new Chambre(14,1,30),
				new Chambre(15,4,90),
				new Chambre(16,2,80),
				new Chambre(17,4,390),
				new Chambre(18,2,20),
				new Chambre(19,2,90),
				new Chambre(20,1,30),
				new Chambre(21,2,130)
		));
		
		/*CREATION DES HOTELS */
		hotels = new ArrayList<>();
		hotels.addAll(Arrays.asList(
				new Hotel(0,"HôtelF1", "France", "Montpellier", "Avenue de Saint-Hubert",
						3,-0.785369, 5.73247843,new ArrayList<Chambre>(Arrays.asList(chambres.get(0),chambres.get(1),chambres.get(2)))),
				new Hotel(1,"Hôtel Transcontinental", "France", "Paris", "Avenue Nina-Simone",
						2,-0.97542, 9.345324324,new ArrayList<Chambre>(Arrays.asList(chambres.get(3),chambres.get(4)))),
				new Hotel(2,"Levante Club Resort", "Espagne", "Madrid", "Avenue de Estocolmo",
						5,-7.54643342, 2.7456543,new ArrayList<Chambre>(Arrays.asList(chambres.get(5)))),
				new Hotel(3,"Grand Hôtel du Lac", "Suisse", "Genève", "Rue d'Italie 1",
						1,9.3269, -5.73247843,new ArrayList<Chambre>(Arrays.asList(chambres.get(6),chambres.get(7),chambres.get(8)))),
				new Hotel(4,"Kaza Tropikal", "France", "Paris", "Route de Ganges",
						1,-9.785369, 2.73247843,new ArrayList<Chambre>(Arrays.asList(chambres.get(9),chambres.get(10)))),
				new Hotel(5,"Domaine de Verchant", "France", "Montpellier", "Place de la Comédie",
						3,7.354369, 1.342432,new ArrayList<Chambre>(Arrays.asList(chambres.get(11),chambres.get(12),chambres.get(13)))),
				new Hotel(6,"Martin's Château du Lac", "Belgique", "Bruxelles", "Avenue du Lac 87",
						1,-5.2456, 3.2456,new ArrayList<Chambre>(Arrays.asList(chambres.get(14),chambres.get(15)))),
				new Hotel(7,"Sonnenalp Resort", "Allemagne", "Berlin", "Fontenay 10",
						5,-5.785369, 6.87654,new ArrayList<Chambre>(Arrays.asList(chambres.get(16)))),
				new Hotel(8,"Excelsior Palace Hotel", "Italie", "Rome", "Via Puccini",
						4,-2.5436556, 9.234,new ArrayList<Chambre>(Arrays.asList(chambres.get(17),chambres.get(18),chambres.get(19)))),
				new Hotel(9,"Hôtel de la plage", "France", "Montpellier", "La Grande-Motte",
						2,-2.656469, 11.6436787,new ArrayList<Chambre>(Arrays.asList(chambres.get(20),chambres.get(21))))	
				
		));
		
		/*AJOUT DE DATES DE RESERVATION DEJA EXISTANTE DANS CERTAINES CHAMBRES */
		
	
		hotels.get(0).getListeChambres().get(0).addUneReservation(LocalDateTime.of(2023, 11, 20, 13, 30),LocalDateTime.of(2023, 11, 27, 18, 15));
		hotels.get(3).getListeChambres().get(0).addUneReservation(LocalDateTime.of(2023, 11, 13, 8, 30),LocalDateTime.of(2023, 11, 15, 15, 30));
		hotels.get(5).getListeChambres().get(2).addUneReservation(LocalDateTime.of(2023, 12, 25, 8, 30),LocalDateTime.of(2023, 12, 31, 10, 30));
		
	}
	

	public HashMap<Hotel, ArrayList<Chambre>>  recherche(String villeSejour, LocalDateTime dateArrive,
			LocalDateTime dateDepart, float prixMin, float prixMax, int categHotel, int nbrPersonne) {
		
		HashMap<Hotel, ArrayList<Chambre>>  hotelsChambres = new HashMap<Hotel, ArrayList<Chambre>> ();
		
		for (Hotel h : hotels) {
			if (categHotel == h.getCategHotel()) { //Verif categorie de l'hotel
				
				if (villeSejour.equals(h.getVille())) { //Verif ville de l'hotel
					
	            	for (Chambre c : h.getListeChambres()) {//Pour chaque chambre de l'hotel
	            		
	            		if (nbrPersonne == c.getNbrLits()) {//Verif nombre de lits
	            			
	            			if(c.getPrix() >= prixMin && c.getPrix() <= prixMax) {//Verif intervalle prix

	            				if (c.estDisponible(dateArrive,dateDepart)) {//Verif disponibilitées horaires
	            					
	            					if (hotelsChambres.get(h) != null) { //Si la clé existe deja ,on ajoute une chambre à sa liste
	            						ArrayList<Chambre> listchambres = new ArrayList<Chambre>() ;
	            						listchambres=hotelsChambres.get(h);
	            						listchambres.add(c);
	            						
		            					hotelsChambres.put(h,listchambres);
	            					}
	            					else {//Sinon on initialise la HashMap avec la clé et une liste contenant la chambre
		            					hotelsChambres.put(h,new ArrayList<Chambre>(Arrays.asList(c)));
	            					}
	    	            		}	            				
	            			}	            			
	            		}
	            	}
	            }
			}            
        }

		return hotelsChambres;
	}


	public boolean reserve(Client client,Chambre chambre, LocalDateTime dateDebut, LocalDateTime dateFin, String codeCB) {
		chambre.addUneReservation(dateDebut,dateFin);
		return true;
	}
	
	/* ACCESSEURS ET MUTATEURS POUR JUNIT */
	public List<Hotel> getHotels() {
		return hotels;
	}


	public void setHotels(List<Hotel> hotels) {
		this.hotels = hotels;
	}


	public List<Chambre> getChambres() {
		return chambres;
	}


	public void setChambres(List<Chambre> chambres) {
		this.chambres = chambres;
	}

}
