package model;

import java.util.ArrayList;

public class Hotel {
	/* ATTRIBUTES */
	private int idHotel;
	private String nom;
	private String pays;
	private String ville;
	private String rue;
	private int categHotel;
	private double coordX;
	private double coordY;
	private ArrayList<Chambre> listeChambres;
	
	/* CONSTRUCTORS */
	public Hotel(int idHotel,String nom, String pays, String ville, String rue, int categHotel,
			double coordX, double coordY, ArrayList<Chambre> listeChambres) {
		super();
		this.idHotel = idHotel;
		this.nom = nom;
		this.pays = pays;
		this.ville = ville;
		this.rue = rue;
		this.setCategHotel(categHotel);
		this.coordX = coordX;
		this.coordY = coordY;
		this.setListeChambres(listeChambres);
	}
	
	/* METHODS */
	public int getIdHotel() {
		return idHotel;
	}

	public void setIdHotel(int idHotel) {
		this.idHotel = idHotel;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public int getCategHotel() {
		return categHotel;
	}

	public void setCategHotel(int categHotel) {
		this.categHotel = categHotel;
	}

	public double getCoordX() {
		return coordX;
	}

	public void setCoordX(double coordX) {
		this.coordX = coordX;
	}

	public double getCoordY() {
		return coordY;
	}

	public void setCoordY(double coordY) {
		this.coordY = coordY;
	}

	public ArrayList<Chambre> getListeChambres() {
		return listeChambres;
	}

	public void setListeChambres(ArrayList<Chambre> listeChambres) {
		this.listeChambres = listeChambres;
	}


	
}
