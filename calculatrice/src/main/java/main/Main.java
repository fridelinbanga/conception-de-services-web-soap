package main;

import java.net.MalformedURLException;
import java.net.URL;

import calculatrice.Calculator;
import calculatrice.CalculatorSoap;


public class Main {

	public static void main(String[] args) {
		
		try {
			URL url = new URL("http://www.dneonline.com/calculator.asmx?WSDL");
			Calculator serviceImpl = new Calculator(url);
			CalculatorSoap proxy = serviceImpl.getCalculatorSoap();
			
			int a = 20; int b =5;
			System.out.println(a+" + "+b +" = "+proxy.add(a, b));
			System.out.println(a+" - "+b +" = "+proxy.subtract(a, b) );
			System.out.println(a+" * "+b +" = "+proxy.multiply(a, b) );
			System.out.println(a+" / "+b +" = "+proxy.divide(a, b) );
			
			b=0;
			try {
				System.out.println(a+" / "+b +" = "+proxy.divide(a, b) );
			} catch (Exception e) {
				System.err.println("Exception détectée : division par 0");
			}
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
