package service;


import javax.jws.WebMethod;
import javax.jws.WebService;

import exceptions.ErreurReservation;
import exceptions.WrongIdOrPassword;

@WebService
public interface ReserveHotelService {
	
	@WebMethod
	public String reserve(int idAgence, String mdpAgence,int idOffre,String nomClient,String prenomClient,String codeCB) throws WrongIdOrPassword,ErreurReservation; 
}
