package versionNonDistribue;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import exceptions.WrongIdOrPassword;
import model.Hotel;
import repository.HotelRepository;

class TestVerifConnexion {
	
	HotelRepository repo= new HotelRepository();

	@Test
	void testVerifConnexion() throws WrongIdOrPassword {
		
		assertTrue(repo.verifConnexion(1, "123",repo.getHotels().get(0))); //Valide
		assertTrue(repo.verifConnexion(1, "098",repo.getHotels().get(9))); //Valide
		assertTrue(repo.verifConnexion(2, "567",repo.getHotels().get(2))); //Valide
		assertTrue(repo.verifConnexion(3, "789",repo.getHotels().get(5))); //Valide
		
		Exception exception = assertThrows(WrongIdOrPassword.class, () -> {
			repo.verifConnexion(1, "124",repo.getHotels().get(0)); //On entre mauvais mot de passe
		});
		
		String expectedMessage = "L'identifiant ou le mot de passe n'est pas reconnu";
		String actualMessage = exception.getMessage();
		
		assertTrue(actualMessage.contains(expectedMessage));

		
	}

}
