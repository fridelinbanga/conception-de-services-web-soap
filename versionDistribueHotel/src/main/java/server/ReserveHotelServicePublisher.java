package server;

import javax.xml.ws.Endpoint;

import model.Hotel;
import service.RechercheHotelServiceImpl;
import service.ReserveHotelServiceImpl;
import repository.HotelRepository;

public class ReserveHotelServicePublisher {
	public static void main(String[] args) {
		
		HotelRepository repo = new HotelRepository();
		int portDepart=8080;
		for (Hotel h : repo.getHotels()) { //On arrete la boucle à 3 hôtels sinon c'est trop long à implémenter	
			if (portDepart==8083) {
				break;
			}
			
			Endpoint.publish("http://localhost:"+Integer.toString(portDepart)+"/recherchehotelservicepublisher",new RechercheHotelServiceImpl(h));
			Endpoint.publish("http://localhost:"+Integer.toString(portDepart)+"/reservehotelservicepublisher",new ReserveHotelServiceImpl(h));
			System.out.println("\n");
			System.out.println("Serveur créer pour l'hôtel \""+h.getNom()+"\"");
			System.out.println("\n-------------------------------------------------------------------------------");
			portDepart+=1;
		}
		
		
	}
}
