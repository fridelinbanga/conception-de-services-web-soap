package hotel0;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the hotel0 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ReserveResponse_QNAME = new QName("http://service/", "reserveResponse");
    private final static QName _ErreurReservation_QNAME = new QName("http://service/", "ErreurReservation");
    private final static QName _WrongIdOrPassword_QNAME = new QName("http://service/", "WrongIdOrPassword");
    private final static QName _Reserve_QNAME = new QName("http://service/", "reserve");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: hotel0
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReserveResponse }
     * 
     */
    public ReserveResponse createReserveResponse() {
        return new ReserveResponse();
    }

    /**
     * Create an instance of {@link Reserve }
     * 
     */
    public Reserve createReserve() {
        return new Reserve();
    }

    /**
     * Create an instance of {@link WrongIdOrPassword }
     * 
     */
    public WrongIdOrPassword createWrongIdOrPassword() {
        return new WrongIdOrPassword();
    }

    /**
     * Create an instance of {@link ErreurReservation }
     * 
     */
    public ErreurReservation createErreurReservation() {
        return new ErreurReservation();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReserveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "reserveResponse")
    public JAXBElement<ReserveResponse> createReserveResponse(ReserveResponse value) {
        return new JAXBElement<ReserveResponse>(_ReserveResponse_QNAME, ReserveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ErreurReservation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "ErreurReservation")
    public JAXBElement<ErreurReservation> createErreurReservation(ErreurReservation value) {
        return new JAXBElement<ErreurReservation>(_ErreurReservation_QNAME, ErreurReservation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WrongIdOrPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "WrongIdOrPassword")
    public JAXBElement<WrongIdOrPassword> createWrongIdOrPassword(WrongIdOrPassword value) {
        return new JAXBElement<WrongIdOrPassword>(_WrongIdOrPassword_QNAME, WrongIdOrPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Reserve }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "reserve")
    public JAXBElement<Reserve> createReserve(Reserve value) {
        return new JAXBElement<Reserve>(_Reserve_QNAME, Reserve.class, null, value);
    }

}
