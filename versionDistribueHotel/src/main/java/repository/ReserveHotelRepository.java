package repository;

import exceptions.ErreurReservation;
import exceptions.WrongIdOrPassword;


public interface ReserveHotelRepository {
		
	public String reserve(int idAgence, String mdpAgence,int idOffre,String nomClient,String prenomClient,String codeCB) throws WrongIdOrPassword,ErreurReservation; 
}
