
## TP2 Architecture Distribuée
## Test de la Calculatrice
Importez le projet Maven "calculatrice" dans Eclipse et mettez à jour le fichier pom.xml. Ajustez la version de Java dans le build path à la version 11 et le compilateur Java à la version 1.8. Une fois l'environnement configuré, exécutez la classe Main pour tester l'application.

## Test de la Version Non Distribuée
Pour tester l'application :

Dans Eclipse, importez le projet Maven versionNonDistribue et exécutez la classe Main dans le package Main.
Remarque : En console, les tests Junit se lancent automatiquement avant la classe Main. Dans Eclipse, vous pouvez les exécuter manuellement ou via Maven (clic droit sur pom.xml > Run as > Maven test).

## Test de la Version Distribuée
Pour tester l'application :

Dans Eclipse, importez les projets Maven versionDistribueAgences et versionDistribueHotels, lancez d'abord le serveur (package server), puis les CLI des Agences (package main).

## Attention : 
Attendez que les serveurs soient actifs avant de lancer les clients, cela peut prendre quelques secondes.

## Configuration Java pour les Modules
Si vous utilisez Java 9 ou une version ultérieure, vous pourriez avoir besoin d'ajouter l'option VM --add-opens java.base/jdk.internal.misc=ALL-UNNAMED pour ouvrir certains packages nécessaires au module java.base. Pour se faire, il faudr suivre ces étapes:
## 1-Ouvrir la Configuration de Lancement :

Cliquez droit sur votre projet dans l'explorateur de package d'Eclipse.
Sélectionnez "Run As" > "Run Configurations..." ou "Debug As" > "Debug Configurations...".
## 2-Modifier la Configuration :

Choisissez votre configuration d'exécution sous "Java Application".
Si nécessaire, créez une nouvelle configuration en cliquant sur "New launch configuration".
## 3-Ajouter des Options VM :

## 4-Allez dans l'onglet "Arguments".
Dans "VM arguments", ajoutez : --add-opens java.base/jdk.internal.misc=ALL-UNNAMED.
Appliquer et Exécuter :

Cliquez sur "Apply" pour enregistrer les modifications.
Cliquez sur "Run" ou "Debug" pour lancer votre application avec ces paramètres.

## Note :
Pour vérifier le transfert des images pour l'exercice 3, videz le dossier resources de versionDistribueAgences avant de démarrer le serveur. Ainsi, vous pourrez voir les images apparaître lors de l'appel au service web de recherche.