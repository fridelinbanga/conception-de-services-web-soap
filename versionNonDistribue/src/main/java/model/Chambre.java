package model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

public class Chambre {
	/* ATTRIBUTES */
	private int idChambre;
	private int nbrLits;
	private float prix; //représente le prix d'une nuit dans la chambre
	private ArrayList<ArrayList<LocalDateTime>> datesReservees;// représente un tableau de couple [DateDebut,DateFin] correspondant aux dates reservées

	/* CONSTRUCTORS */
	public Chambre(int idChambre, int nbrLits, float prix) {
		super();
		this.idChambre = idChambre;
		this.nbrLits = nbrLits;
		this.prix = prix;
		this.datesReservees = new ArrayList<ArrayList<LocalDateTime>>();
	}
	
	/* METHODS */
	public int getIdChambre() {
		return idChambre;
	}
	public void setIdChambre(int idChambre) {
		this.idChambre = idChambre;
	}
	public int getNbrLits() {
		return nbrLits;
	}
	public void setNbrLits(int nbrLits) {
		this.nbrLits = nbrLits;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}

	public ArrayList<ArrayList<LocalDateTime>> getdatesReservees() {
		return datesReservees;
	}

	public void setdatesReservees(ArrayList<ArrayList<LocalDateTime>> datesReservees) {
		this.datesReservees = datesReservees;
	}
	
	public void addUneReservation(LocalDateTime dateDebut,LocalDateTime dateFin) {
		this.datesReservees.add(new ArrayList<LocalDateTime>(Arrays.asList(dateDebut,dateFin)));
	}
	
	
	
	/* L'idée derrière cette méthode est la suivante : 
     * Pour confirmer que le créneau est libre,  
     * il faut que la date de debut et de fin passé en paramètre soit (INFERIEUR A TOUTES les dates de debut) 
     * || (SUPERIEUR A TOUTES les dates de fin) présentent dans les réservations */
	public boolean estDisponible(LocalDateTime dateDebut,LocalDateTime dateFin) {
		
		for (ArrayList<LocalDateTime> tabDates : this.datesReservees) {
            
            if (  !((dateDebut.isBefore(tabDates.get(0)) && dateFin.isBefore(tabDates.get(0))) ||
            	  (dateDebut.isAfter(tabDates.get(1)) && dateFin.isAfter(tabDates.get(1))))  ) {
            	return false;
            }
        }
		return true;
	}
	
}
