package hotel0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for offre complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="offre">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dateArrivee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateDepart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idAgence" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idOffre" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="imgEncode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nbrLits" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="prix" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "offre", propOrder = {
    "dateArrivee",
    "dateDepart",
    "idAgence",
    "idOffre",
    "imgEncode",
    "nbrLits",
    "prix"
})
public class Offre {

    protected String dateArrivee;
    protected String dateDepart;
    protected int idAgence;
    protected int idOffre;
    protected String imgEncode;
    protected int nbrLits;
    protected float prix;

    /**
     * Gets the value of the dateArrivee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateArrivee() {
        return dateArrivee;
    }

    /**
     * Sets the value of the dateArrivee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateArrivee(String value) {
        this.dateArrivee = value;
    }

    /**
     * Gets the value of the dateDepart property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateDepart() {
        return dateDepart;
    }

    /**
     * Sets the value of the dateDepart property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateDepart(String value) {
        this.dateDepart = value;
    }

    /**
     * Gets the value of the idAgence property.
     * 
     */
    public int getIdAgence() {
        return idAgence;
    }

    /**
     * Sets the value of the idAgence property.
     * 
     */
    public void setIdAgence(int value) {
        this.idAgence = value;
    }

    /**
     * Gets the value of the idOffre property.
     * 
     */
    public int getIdOffre() {
        return idOffre;
    }

    /**
     * Sets the value of the idOffre property.
     * 
     */
    public void setIdOffre(int value) {
        this.idOffre = value;
    }

    /**
     * Gets the value of the imgEncode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImgEncode() {
        return imgEncode;
    }

    /**
     * Sets the value of the imgEncode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImgEncode(String value) {
        this.imgEncode = value;
    }

    /**
     * Gets the value of the nbrLits property.
     * 
     */
    public int getNbrLits() {
        return nbrLits;
    }

    /**
     * Sets the value of the nbrLits property.
     * 
     */
    public void setNbrLits(int value) {
        this.nbrLits = value;
    }

    /**
     * Gets the value of the prix property.
     * 
     */
    public float getPrix() {
        return prix;
    }

    /**
     * Sets the value of the prix property.
     * 
     */
    public void setPrix(float value) {
        this.prix = value;
    }

}
