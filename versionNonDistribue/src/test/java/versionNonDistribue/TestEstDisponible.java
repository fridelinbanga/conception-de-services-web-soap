package versionNonDistribue;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import model.Chambre;
import repository.ReserveHotelAppRepository;

class TestEstDisponible {
	
	ReserveHotelAppRepository app = new ReserveHotelAppRepository();
	
	
	/* Pour l'ensemble de ces tests, le créneau réservé est le suivant : 2023-11-20T13:30 -> 2022-11-25T13:30 */
	Chambre c = app.getHotels().get(0).getListeChambres().get(0);
	
	LocalDateTime dateArrive = LocalDateTime.of(2023, 10, 20, 13, 30);
	LocalDateTime dateDepart = LocalDateTime.of(2023, 10, 25, 13, 30);	
	
	/* Pour bien visualiser les différents cas de figure : le premier 0 correspond à l'argument 
	 * dateDebut et le second 0 à l'argument dateFin de la méthode EstDisponible,
	 * de même pour les 1 avec la date de début et la date de fin d'une dateRéservée
	 * 
	 * 0 0 1 1 => Validé par la 1er condition du || de la méthode EstDisponible
	 * 1 1 0 0 => Validé par la 2eme condition du || de la méthode EstDisponible
	 * 1 0 0 1 => Invalidé par les 2 conditions du || de la méthode EstDisponible
	 * 1 0 1 0 => Invalidé par les 2 conditions du || de la méthode EstDisponible
     * 0 1 0 1 => Invalidé par les 2 conditions du || de la méthode EstDisponible
     * 0 1 1 0 => Invalidé par les 2 conditions du || de la méthode EstDisponible
     * */
	
	@Test
	void estDisponible_Cas_0_0_1_1() {	
		assertTrue(c.estDisponible(dateArrive,dateDepart));
	}
	
	@Test
	void estDisponible_Cas_1_1_0_0() {
		dateArrive = LocalDateTime.of(2023, 12, 20, 13, 30);
		dateDepart = LocalDateTime.of(2023, 12, 25, 13, 30);
		assertTrue(c.estDisponible(dateArrive,dateDepart));
	}
	
	@Test
	void estDisponible_Cas_0_1_1_0() {
		dateArrive = LocalDateTime.of(2023, 10, 20, 13, 30);
		dateDepart = LocalDateTime.of(2023, 12, 25, 13, 30);
		assertFalse(c.estDisponible(dateArrive,dateDepart));
	}
	
	@Test
	void estDisponible_Cas_1_0_0_1() {
		dateArrive = LocalDateTime.of(2023, 11, 21, 13, 30);
		dateDepart = LocalDateTime.of(2023, 11, 24, 13, 30);
		assertFalse(c.estDisponible(dateArrive,dateDepart));
	}
	
	@Test
	void estDisponible_Cas_0_1_0_1() {
		dateArrive = LocalDateTime.of(2023, 10, 21, 13, 30);
		dateDepart = LocalDateTime.of(2023, 11, 24, 13, 30);
		assertFalse(c.estDisponible(dateArrive,dateDepart));
	}
	

	@Test
	void estDisponible_Cas_1_0_1_0() {
		dateArrive = LocalDateTime.of(2023, 11, 21, 13, 30);
		dateDepart = LocalDateTime.of(2023, 12, 24, 13, 30);
		assertFalse(c.estDisponible(dateArrive,dateDepart));
	}
	

	@Test
	@DisplayName("Cas de figure où il y a deux créneaux différents déjà reservé pour la chambre")
	void estDisponible_Cas_1_bon_1_mauvais() {
		
		//ajout d'un deuxieme créneau
		c.addUneReservation(LocalDateTime.of(2023, 12, 20, 13, 30),LocalDateTime.of(2023, 12, 27, 18, 15));
		
		dateArrive = LocalDateTime.of(2023, 10, 20, 13, 30);
		dateDepart = LocalDateTime.of(2023, 10, 25, 13, 30);	
		assertTrue(c.estDisponible(dateArrive,dateDepart));
		
		dateArrive = LocalDateTime.of(2023, 11, 21, 13, 30);
		dateDepart = LocalDateTime.of(2023, 11, 24, 13, 30);	
		assertFalse(c.estDisponible(dateArrive,dateDepart));
		
		dateArrive = LocalDateTime.of(2023, 12, 21, 13, 30);
		dateDepart = LocalDateTime.of(2023, 12, 24, 13, 30);	
		assertFalse(c.estDisponible(dateArrive,dateDepart));
	}
	
}
