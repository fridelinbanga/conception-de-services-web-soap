package repository;

import java.util.ArrayList;
import java.util.Arrays;

import exceptions.AucunHotelTrouve;
import model.InfosConnuesParLAgence;

public class AgenceRepository {
	
	private ArrayList<InfosConnuesParLAgence> nomVilleHotelAgence1; 
	private ArrayList<InfosConnuesParLAgence> nomVilleHotelAgence2; 
	
	public AgenceRepository() {
		nomVilleHotelAgence1=new  ArrayList<InfosConnuesParLAgence>();
		nomVilleHotelAgence1.addAll((Arrays.asList(
				new InfosConnuesParLAgence(0,"123","HôtelF1", "France", "Montpellier", "Avenue de Saint-Hubert",
						"http://localhost:8080/recherchehotelservicepublisher?wsdl","http://localhost:8080/reservehotelservicepublisher?wsdl",1.10f),
				new InfosConnuesParLAgence(1,"345","Hôtel Transcontinental", "France", "Paris", "Avenue Nina-Simone",
						"http://localhost:8081/recherchehotelservicepublisher?wsdl","http://localhost:8081/reservehotelservicepublisher?wsdl",1.13f),
				new InfosConnuesParLAgence(2,"567","Grand Hôtel du Lac", "Suisse", "Genève", "Rue d'Italie 1",
						"http://localhost:8082/recherchehotelservicepublisher?wsdl","http://localhost:8082/reservehotelservicepublisher?wsdl",1.15f)
				)));	
		
		nomVilleHotelAgence2=new  ArrayList<InfosConnuesParLAgence>();
		nomVilleHotelAgence2.addAll((Arrays.asList(
				new InfosConnuesParLAgence(0,"345","HôtelF1", "France", "Montpellier", "Avenue de Saint-Hubert",
						"http://localhost:8080/recherchehotelservicepublisher?wsdl","http://localhost:8080/reservehotelservicepublisher?wsdl",1.25f),
				new InfosConnuesParLAgence(1,"123","Hôtel Transcontinental", "France", "Paris", "Avenue Nina-Simone",
						"http://localhost:8081/recherchehotelservicepublisher?wsdl","http://localhost:8081/reservehotelservicepublisher?wsdl",1.20f),
				new InfosConnuesParLAgence(2,"567","Grand Hôtel du Lac", "Suisse", "Genève", "Rue d'Italie 1",
						"http://localhost:8082/recherchehotelservicepublisher?wsdl","http://localhost:8082/reservehotelservicepublisher?wsdl",1.18f)
				)));
	}
	
	public ArrayList<InfosConnuesParLAgence> hotelsPartenairesAgence(int idAgence,String ville) throws AucunHotelTrouve{
		
		 ArrayList<InfosConnuesParLAgence> infosRenvoyees = new ArrayList<InfosConnuesParLAgence>(); 
		
		switch(idAgence) {
		case 1:
			for (InfosConnuesParLAgence i : nomVilleHotelAgence1) {
				if (i.getVilleHotel().equals(ville)) {
					infosRenvoyees.add(i);
				}
				
			}
			return infosRenvoyees;
		case 2:
			for (InfosConnuesParLAgence i : nomVilleHotelAgence2) {
				if (i.getVilleHotel().equals(ville)) {
					infosRenvoyees.add(i);
				}
			
			}
			return infosRenvoyees;		
		}
		throw new AucunHotelTrouve("Aucun hôtel n'a été trouvé dans cette ville");

	
	}

}
