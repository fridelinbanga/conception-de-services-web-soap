package repository;

import java.time.LocalDateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import exceptions.WrongIdOrPassword;
import model.Agence;
import model.Chambre;
import model.Hotel;
import model.Offre;
import model.EstPartenaire;

public class HotelRepository {
	
	/* ATTRIBUTES */
	protected List<Hotel> hotels ;
	protected List<Chambre> chambres ;
	protected List<Agence> agences;
	protected List<EstPartenaire> partenaires;

	/* CONSTRUCTOR */
	public HotelRepository() {
		
		/*CREATION DES CHAMBRES */
		chambres = new ArrayList<>();
		chambres.addAll(Arrays.asList(
				new Chambre(0,2,90,"chambre0"),
				new Chambre(1,4,120,"chambre1"),
				new Chambre(2,1,30,"chambre2"),
				new Chambre(3,2,40,"chambre3"),
				new Chambre(4,2,40,"chambre4"),
				new Chambre(5,1,30,"chambre5"),
				new Chambre(6,4,90,"chambre6"),
				new Chambre(7,2,60,"chambre7"),
				new Chambre(8,1,90,"chambre8"),
				new Chambre(9,1,70,""),
				new Chambre(10,2,90,""),
				new Chambre(11,2,90,""),
				new Chambre(12,4,330,""),
				new Chambre(13,2,80,""),
				new Chambre(14,1,30,""),
				new Chambre(15,4,90,""),
				new Chambre(16,2,80,""),
				new Chambre(17,4,390,""),
				new Chambre(18,2,20,""),
				new Chambre(19,2,90,""),
				new Chambre(20,1,30,""),
				new Chambre(21,2,130,""),
				
				new Chambre(22,2,330,"chambre22"),
				new Chambre(23,2,290,"chambre23"),
				new Chambre(24,4,70,"chambre24"),
				new Chambre(25,2,90,"chambre25"),
				new Chambre(26,1,30,"chambre26"),
				new Chambre(27,4,630,"chambre27")
		));
		
		/*CREATION DES HOTELS */
		hotels = new ArrayList<>();
		hotels.addAll(Arrays.asList(
				new Hotel(0,"HôtelF1", "France", "Montpellier", "Avenue de Saint-Hubert",
						3,-0.785369, 5.73247843,new ArrayList<Chambre>(Arrays.asList(chambres.get(0),chambres.get(1),chambres.get(2),chambres.get(22),chambres.get(23)))),
				new Hotel(1,"Hôtel Transcontinental", "France", "Paris", "Avenue Nina-Simone",
						2,-0.97542, 9.345324324,new ArrayList<Chambre>(Arrays.asList(chambres.get(3),chambres.get(4),chambres.get(24),chambres.get(25),chambres.get(26),chambres.get(27)))),
				new Hotel(2,"Levante Club Resort", "Espagne", "Madrid", "Avenue de Estocolmo",
						5,-7.54643342, 2.7456543,new ArrayList<Chambre>(Arrays.asList(chambres.get(5)))),
				new Hotel(3,"Grand Hôtel du Lac", "Suisse", "Genève", "Rue d'Italie 1",
						1,9.3269, -5.73247843,new ArrayList<Chambre>(Arrays.asList(chambres.get(6),chambres.get(7),chambres.get(8)))),
				new Hotel(4,"Kaza Tropikal", "France", "Paris", "Route de Ganges",
						1,-9.785369, 2.73247843,new ArrayList<Chambre>(Arrays.asList(chambres.get(9),chambres.get(10)))),
				new Hotel(5,"Domaine de Verchant", "France", "Montpellier", "Place de la Comédie",
						3,7.354369, 1.342432,new ArrayList<Chambre>(Arrays.asList(chambres.get(11),chambres.get(12),chambres.get(13)))),
				new Hotel(6,"Loge Volvant", "Belgique", "Bruxelles", "Rue du Ranelagh",
						1,-5.2456, 3.2456,new ArrayList<Chambre>(Arrays.asList(chambres.get(14),chambres.get(15)))),
				new Hotel(7,"Martin's Château du Lac", "Belgique", "Bruxelles", "Avenue du Lac 87",
						5,-5.785369, 6.87654,new ArrayList<Chambre>(Arrays.asList(chambres.get(16)))),
				new Hotel(8,"Sonnenalp Resort", "Allemagne", "Berlin", "Fontenay 10",
						4,-2.5436556, 9.234,new ArrayList<Chambre>(Arrays.asList(chambres.get(17),chambres.get(18),chambres.get(19)))),
				new Hotel(9,"Excelsior Palace Hotel", "Italie", "Rome", "Via Puccini",
						2,-2.656469, 11.6436787,new ArrayList<Chambre>(Arrays.asList(chambres.get(20),chambres.get(21))))	
				
		));
				
		/*AJOUT DE DATES DE RESERVATION DEJA EXISTANTE DANS CERTAINES CHAMBRES */
		
		
		hotels.get(0).getListeChambres().get(0).addUneReservation(LocalDateTime.of(2023, 11, 20, 13, 30),LocalDateTime.of(2023, 11, 27, 18, 15));
		hotels.get(3).getListeChambres().get(0).addUneReservation(LocalDateTime.of(2023, 11, 13, 8, 30),LocalDateTime.of(2023, 11, 15, 15, 30));
		hotels.get(5).getListeChambres().get(2).addUneReservation(LocalDateTime.of(2023, 12, 25, 8, 30),LocalDateTime.of(2023, 12, 31, 10, 30));
		
		 
		/*CREATION DES AGENCES*/		
		agences = new ArrayList<>();
		agences.addAll(Arrays.asList(
				new Agence(1,"Agence1"),
				new Agence(2,"Agence2"),
				new Agence(3,"Agence3")
		));		
		
		/*AFFECTATION DES AGENCES PARTENAIRES*/
		partenaires = new ArrayList<>();
		partenaires.addAll((Arrays.asList(
				
				//Agence 1
				new EstPartenaire(agences.get(0),"123", hotels.get(0)),
				new EstPartenaire(agences.get(0),"345", hotels.get(1)),
				new EstPartenaire(agences.get(0),"567", hotels.get(2)),
				new EstPartenaire(agences.get(0),"789", hotels.get(3)),
				new EstPartenaire(agences.get(0),"901", hotels.get(4)),
				new EstPartenaire(agences.get(0),"234", hotels.get(5)),
				new EstPartenaire(agences.get(0),"456", hotels.get(6)),
				new EstPartenaire(agences.get(0),"678", hotels.get(7)),
				new EstPartenaire(agences.get(0),"890", hotels.get(8)),
				new EstPartenaire(agences.get(0),"098", hotels.get(9)),
				
				//Agence 2
				new EstPartenaire(agences.get(1),"345", hotels.get(0)),
				new EstPartenaire(agences.get(1),"123", hotels.get(1)),
				new EstPartenaire(agences.get(1),"567", hotels.get(2)),
				new EstPartenaire(agences.get(1),"654", hotels.get(3)),
				new EstPartenaire(agences.get(1),"132", hotels.get(4)),
				
				//Agence 3
				new EstPartenaire(agences.get(2),"789", hotels.get(5)),
				new EstPartenaire(agences.get(2),"231", hotels.get(6)),
				new EstPartenaire(agences.get(2),"158", hotels.get(7)),
				new EstPartenaire(agences.get(2),"376", hotels.get(8)),
				new EstPartenaire(agences.get(2),"132", hotels.get(9))
		)));	
		
		/* INITILISATION DE LA HASHMAP offres POUR LA PERSISTANCE DES DONNÉES*/
		// A chaque fois qu'on renverra une offre avec la méthode recherche on l'ajoutera à la liste des offres d'un hôtel
		// Lorsqu'une agence aura reserver avec un idOffre, ses offres seront retirés de la liste.
		
	}
	
	/* METHODS */
	public List<Hotel> getHotels() {
		return hotels;
	}

	public void setHotels(List<Hotel> hotels) {
		this.hotels = hotels;
	}

	public List<Chambre> getChambres() {
		return chambres;
	}


	public void setChambres(List<Chambre> chambres) {
		this.chambres = chambres;
	}
	
	public List<Agence> getAgences() {
		return agences;
	}

	public void setAgences(List<Agence> agences) {
		this.agences = agences;
	}
	public List<EstPartenaire> getPartenaires() {
		return partenaires;
	}

	public void setPartenaires(List<EstPartenaire> partenaires) {
		this.partenaires = partenaires;
	}
	
	public boolean verifConnexion(int idAgence, String mdpAgence,Hotel hotel) throws WrongIdOrPassword {
		
		for (EstPartenaire p : partenaires) {
			if (p.getHotel().getIdHotel() == hotel.getIdHotel()) {
				if (p.getAgence().getId() == idAgence && p.getMdp().equals(mdpAgence)) {
					return true;					
				}
			}
		}
		throw new WrongIdOrPassword("L'identifiant ou le mot de passe n'est pas reconnu");	
		
	}
}
