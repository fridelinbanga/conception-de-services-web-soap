package service;

import javax.jws.WebService;

import exceptions.ErreurReservation;
import exceptions.WrongIdOrPassword;
import model.Hotel;
import repository.ReserveHotelRepository;
import repository.ReserveHotelRepositoryImpl;

@WebService(endpointInterface="service.ReserveHotelService")
public class ReserveHotelServiceImpl implements ReserveHotelService {
	
	/* ATTRIBUTES */
	private Hotel hotel;
	
	public ReserveHotelServiceImpl(Hotel hotel) {
		this.hotel=hotel;
	}
	
	
	@Override
	public String reserve(int idAgence, String mdpAgence,int idOffre,String nomClient,String prenomClient,String codeCB) throws WrongIdOrPassword,ErreurReservation {
		ReserveHotelRepository repository = new ReserveHotelRepositoryImpl(this.hotel);
		return repository.reserve(idAgence,mdpAgence,idOffre,nomClient,prenomClient,codeCB);
	}
	

}
