package versionNonDistribue;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import main.Regex;

public class TestRegex {
	
	Regex rg=new Regex();
	
	@Test
	void estAlphabetique() {
		assertTrue(rg.estAlphabetique("Montpellier"));
		assertTrue(rg.estAlphabetique("Ville avec espace"));
		
		assertFalse(rg.estAlphabetique("Mont3pellier"));
		assertFalse(rg.estAlphabetique("Mont-pellier"));
		assertFalse(rg.estUneDate(""));
	}
	
	@Test
	void estUneDate() {
		assertTrue(rg.estUneDate("20/12/2023 13:33"));
		
		assertFalse(rg.estUneDate("20/12/2022 13:33")); // Date inférieur à 2023 donc False pour notre réservation d'hôtel
		assertFalse(rg.estUneDate("20/13/2023 13:33")); // Date avec un mois supérieur à 12
		assertFalse(rg.estUneDate("34/12/2023 13:33")); // Date avec un jour supérieur à 31
		assertFalse(rg.estUneDate("34/12/2023 24:33")); // Date avec une heure supérieur à 23
		assertFalse(rg.estUneDate("34/12/2023 24:60")); // Date avec une minute supérieur à 59
		
		assertFalse(rg.estUneDate("2/12/2023 13:33")); 
		assertFalse(rg.estUneDate("20/2/2023 13:33"));
		assertFalse(rg.estUneDate("20/2/2023 3:33"));
		assertFalse(rg.estUneDate("20/2/2023 33:3"));
		assertFalse(rg.estUneDate("sfdsdsf"));
		assertFalse(rg.estUneDate(""));
	}
	
	@Test
	void estUnCodeCB() {
		assertTrue(rg.estUnCodeCB("1234567890123456 302 11/23"));
		
		assertFalse(rg.estUnCodeCB("12345637890123456 302 11/23"));
		assertFalse(rg.estUnCodeCB("123456e789012456 302 11/23"));
		assertFalse(rg.estUnCodeCB("1234563789012456 302 13/23"));
		assertFalse(rg.estUnCodeCB("1234567789012456 302 12/34"));
		assertFalse(rg.estUnCodeCB("1234561789042456302 11/23"));

	}
}
