package model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

public class Chambre {
	/* ATTRIBUTES */
	private int idChambre;
	private int nbrLits;
	private float prix; //représente le prix d'une nuit dans la chambre
	private ArrayList<ArrayList<LocalDateTime>> datesReservees;// représente un tableau de couple [DateDebut,DateFin] correspondant aux dates reservées
	private String nomImage;
	
	/* CONSTRUCTORS */
	public Chambre(int idChambre, int nbrLits, float prix,String nomImage) {
		super();
		this.idChambre = idChambre;
		this.nbrLits = nbrLits;
		this.prix = prix;
		this.datesReservees = new ArrayList<ArrayList<LocalDateTime>>();
		this.nomImage=nomImage;
	}
	
	/* METHODS */
	public int getIdChambre() {
		return idChambre;
	}
	public void setIdChambre(int idChambre) {
		this.idChambre = idChambre;
	}
	public int getNbrLits() {
		return nbrLits;
	}
	public void setNbrLits(int nbrLits) {
		this.nbrLits = nbrLits;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}

	public ArrayList<ArrayList<LocalDateTime>> getdatesReservees() {
		return datesReservees;
	}

	public void setdatesReservees(ArrayList<ArrayList<LocalDateTime>> datesReservees) {
		this.datesReservees = datesReservees;
	}
	
	public void addUneReservation(LocalDateTime dateDebut,LocalDateTime dateFin) {
		this.datesReservees.add(new ArrayList<LocalDateTime>(Arrays.asList(dateDebut,dateFin)));
	}
	
	
	
	
	public boolean estDisponible(LocalDateTime dateDebut,LocalDateTime dateFin) {
		
		for (ArrayList<LocalDateTime> tabDates : this.datesReservees) {
            
            if (  !((dateDebut.isBefore(tabDates.get(0)) && dateFin.isBefore(tabDates.get(0))) ||
            	  (dateDebut.isAfter(tabDates.get(1)) && dateFin.isAfter(tabDates.get(1))))  ) {
            	return false;
            }
        }
		return true;
	}

	public String getNomImage() {
		return nomImage;
	}

	public void setNomImage(String nomImage) {
		this.nomImage = nomImage;
	}
	
}
