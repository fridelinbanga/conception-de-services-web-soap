/*Ces testes ne sont plus d'actualité*/


/*package versionNonDistribue;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import exceptions.ErreurReservation;
import exceptions.WrongIdOrPassword;
import model.Chambre;
import model.Hotel;
import model.Offre;
import model.ParseLocalDateTime;
import repository.HotelRepository;
import repository.ReserveHotelRepositoryImpl;

class TestReserve {
	
	
	HotelRepository repo= new HotelRepository();
	ReserveHotelRepositoryImpl test ;
	HashMap<Hotel,ArrayList<Offre>> offre;
	
	ParseLocalDateTime pDT = new ParseLocalDateTime();
	
	@BeforeEach
	public void init() {
		test = new ReserveHotelRepositoryImpl(repo.getHotels().get(0));
		Chambre chambre0 = test.getChambres().get(0);
		Chambre chambre1 = test.getChambres().get(1);
		Chambre chambre2 = test.getChambres().get(2);
		Chambre chambre3 = test.getChambres().get(3);
		Chambre chambre4 = test.getChambres().get(4);
		
		offre = new HashMap<>();
		offre.put(test.getHotels().get(0),
				new ArrayList<Offre>(Arrays.asList(new Offre(chambre0.getIdChambre()
						,chambre0.getNbrLits(),pDT.DateToString(LocalDateTime.of(2023, 11, 20, 13, 30)),
						pDT.DateToString(LocalDateTime.of(2023, 11, 22, 13, 30)),chambre0.getPrix(),1),
						
						new Offre(chambre1.getIdChambre()
								,chambre1.getNbrLits(),pDT.DateToString(LocalDateTime.of(2023, 10, 20, 13, 30)),
								pDT.DateToString(LocalDateTime.of(2023, 10, 22, 13, 30)),chambre1.getPrix(),1),
						
						new Offre(chambre2.getIdChambre()
								,chambre2.getNbrLits(),pDT.DateToString(LocalDateTime.of(2023, 12, 20, 13, 30)),
								pDT.DateToString(LocalDateTime.of(2023, 12, 22, 13, 30)),chambre2.getPrix(),1),
						
						new Offre(chambre0.getIdChambre()
								,chambre0.getNbrLits(),pDT.DateToString(LocalDateTime.of(2023, 9, 20, 13, 30)),
								pDT.DateToString(LocalDateTime.of(2023, 9, 22, 13, 30)),chambre0.getPrix(),2)
						
						)));
		
		//Maintenant on ajoute des valeurs pour l'hotel avec l'id 1 , mais attention on ne va pas utiliser ces valeurs 
		// * dans nos tests car ReserveHotelRepositoryImpl a été initialisé avec l'hotel d'id 0 uniquement.
		 //* Ces valeurs servent justent à montrer que notre programme fonctionne quand le repository possède plusieurs clés différentes
		 //* (ceux qui va arrivé dans la réalité vue que la persistance des données des offres de tous les hôtels est stocké dans la 
		 //* même HashMap 
		offre.put(test.getHotels().get(1),
				new ArrayList<Offre>(Arrays.asList(
						new Offre(chambre3.getIdChambre(),chambre3.getNbrLits(),pDT.DateToString(LocalDateTime.of(2023, 11, 20, 13, 30))
								,pDT.DateToString(LocalDateTime.of(2023, 11, 22, 13, 30)),chambre3.getPrix(),1),
						new Offre(chambre3.getIdChambre(),chambre3.getNbrLits(),pDT.DateToString(LocalDateTime.of(2023, 11, 20, 13, 30))
								,pDT.DateToString(LocalDateTime.of(2023, 11, 22, 13, 30)),chambre3.getPrix(),2),
						new Offre(chambre4.getIdChambre(),chambre4.getNbrLits(),pDT.DateToString(LocalDateTime.of(2023, 11, 20, 13, 30))
								,pDT.DateToString(LocalDateTime.of(2023, 11, 22, 13, 30)),chambre4.getPrix(),1),
						new Offre(chambre4.getIdChambre(),chambre4.getNbrLits(),pDT.DateToString(LocalDateTime.of(2023, 11, 20, 13, 30))
								,pDT.DateToString(LocalDateTime.of(2023, 11, 22, 13, 30)),chambre4.getPrix(),3))));
		
		test.setOffres(offre);
	}
	
	@Test
	void testReserve0() throws WrongIdOrPassword,ErreurReservation{
		
		System.out.println("/////////////////// JUNIT DEBUGGING METHODE RESERVE ///////////////////////");
		System.out.println("Contenue de la HashMap : \n");
		for (Hotel h: test.getOffres().keySet()) {
		    String key = h.getNom();
		    System.out.println("Hotel : "+key);
		    for (Offre of :  test.getOffres().get(h)) {
		    	 System.out.println("[idOffre : "+of.getIdOffre()+" ,dateArrivee : "+
		    of.getDateArrivee()+" ,dateDepart : "+of.getDateDepart()+" ,idAgence : "+of.getIdAgence()+"]");
		    }
		}
		System.out.println("\n");
		
		Chambre chambre0_c = test.getChambres().get(0);
		System.out.println("[T1] Creneau réservé de la chambre avant reservation : "+chambre0_c.getdatesReservees());
		System.out.println("Reference : "+test.reserve(1, "123", 0, "jean","jean", "dummyCB"));
		System.out.println("[T1] Creneau réservé de la chambre apres reservation : "+chambre0_c.getdatesReservees());
		System.out.println("\n");
	}
	
	@Test
	void testReserve2() throws WrongIdOrPassword,ErreurReservation{
		Chambre chambre1_c = test.getChambres().get(1);
		System.out.println("[T2] Creneau réservé de la chambre avant reservation : "+chambre1_c.getdatesReservees());
		System.out.println("Reference : "+test.reserve(1, "123", 1, "jean","jean", "dummyCB"));
		System.out.println("[T2] Creneau réservé de la chambre apres reservation : "+chambre1_c.getdatesReservees());
		System.out.println("\n");
	}
	
	@Test
	void testReserve3() throws WrongIdOrPassword,ErreurReservation{
		Chambre chambre2_c = test.getChambres().get(2);
		System.out.println("[T3] Creneau réservé de la chambre avant reservation : "+chambre2_c.getdatesReservees());
		System.out.println("Reference : "+test.reserve(1, "123", 2, "jean","jean", "dummyCB"));
		System.out.println("[T3] Creneau réservé de la chambre apres reservation : "+chambre2_c.getdatesReservees());
		System.out.println("\n");
	}
	
	@Test
	void testReserve4() throws WrongIdOrPassword,ErreurReservation{
		Chambre chambre0_c = test.getChambres().get(0);
		System.out.println("[T4] Creneau réservé de la chambre avant reservation : "+chambre0_c.getdatesReservees());
		System.out.println("Reference : "+test.reserve(2, "345", 0, "jean","jean", "dummyCB"));
		System.out.println("[T4] Creneau réservé de la chambre apres reservation : "+chambre0_c.getdatesReservees());
		System.out.println("\n");
		System.out.println("/////////////////////////////////////////////////////////////////////////////");
	}
	
	@Test
	void testReserve5() throws WrongIdOrPassword,ErreurReservation{
		

		Exception exception = assertThrows(ErreurReservation.class, () -> {
			test.reserve(1, "123", 7, "jean","jean", "dummyCB"); //On entre une mauvaise combinaison id/agence
		});		

		assertEquals("Erreur lors de la réservartion : L'idOffre n'a pas été trouvé", exception.getMessage());
		
		
	}
	
}
*/